#!/usr/bin/env python
# PAP client
import asyncio
import websockets
import hashlib
import hmac


def generate_hmac_sha256(message, key):
    # Преобразование ключа в байтовую строку, если он представлен строкой
    if isinstance(key, str):
        key = key.encode('utf-8')

    # Преобразование сообщения в байтовую строку, если оно представлено строкой
    if isinstance(message, str):
        message = message.encode('utf-8')

    # Создание объекта хеш-функции с использованием алгоритма SHA-256
    hmac_sha256 = hmac.new(key, message, hashlib.sha256)

    # Вычисление HMAC и возвращение его в виде шестнадцатеричной строки
    return hmac_sha256.hexdigest()


async def try_auth(uri):
    async with websockets.connect(uri) as websocket:
        shared_key = "supersecret"
        message = "Anton"

        welcome_message = await websocket.recv()
        print(f"Server answered: {welcome_message}")

        message_HMAC = generate_hmac_sha256(message, shared_key)

        print(f"I send message '{message}' with HMAC '{message_HMAC}' based on password '{shared_key}'")
        client_data = f"{message},{message_HMAC}"
        await websocket.send(client_data)

        answer = await websocket.recv()

        print(answer)

asyncio.get_event_loop().run_until_complete(
    try_auth('ws://localhost:1234')
)
