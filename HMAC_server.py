#!/usr/bin/env python
# PAP server
import asyncio
import websockets
import hashlib
import hmac


def generate_hmac_sha256(message, key):
    # Преобразование ключа в байтовую строку, если он представлен строкой
    if isinstance(key, str):
        key = key.encode('utf-8')

    # Преобразование сообщения в байтовую строку, если оно представлено строкой
    if isinstance(message, str):
        message = message.encode('utf-8')

    # Создание объекта хеш-функции с использованием алгоритма SHA-256
    hmac_sha256 = hmac.new(key, message, hashlib.sha256)

    # Вычисление HMAC и возвращение его в виде шестнадцатеричной строки
    return hmac_sha256.hexdigest()






async def serve(websocket, path):
    shared_key = "supersecret"
    access_granted_message = "Access granted!"
    access_denied_message = "Access denied!"
    hello_message = "Please provide me a comma-separated message,hmac"
    await websocket.send(hello_message)

    client_data = await websocket.recv()
    client_message,client_hmac = client_data.split(",")
    print(f"Got new message '{client_message}' with HMAC '{client_hmac}'")

    server_side_hmac = generate_hmac_sha256(client_message,shared_key)
    print(f"Will compare client_HMAC '{client_hmac}' and server_side_HMAC '{server_side_hmac}' with shared_key '{shared_key}'")
    if client_hmac == server_side_hmac:
        print(access_granted_message)
        await websocket.send(access_granted_message)
    else:
        print(access_denied_message)
        await websocket.send(access_denied_message)


print("Server started!")
start_server = websockets.serve(serve, "localhost", 1234)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
